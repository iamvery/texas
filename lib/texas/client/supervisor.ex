defmodule Texas.Client.Supervisor do
  use DynamicSupervisor

  def start_link do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_client(init_state, client_uid) do
   DynamicSupervisor.start_child(__MODULE__, {Texas.Client.Server, [state: init_state, uid: client_uid]})
  end
end
