defmodule Texas.Plug do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    conn = case conn.cookies["texas_uuid"] do
      nil -> put_new_uuid(conn)
      uuid -> ensure_assigns_has_uuid(conn, uuid)
    end
  end

  defp put_new_uuid(conn) do
    uuid = UUID.uuid4()
    conn
    |> put_resp_cookie("texas_uuid", uuid, [http_only: false])
    |> assign(:texas_uuid, uuid)
  end

  defp ensure_assigns_has_uuid(conn, uuid) do
    assign(conn, :texas_uuid, uuid)
  end
end
