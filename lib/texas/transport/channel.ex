defmodule Texas.Channel do
  use Phoenix.Channel
  @router Application.get_env(:texas, :router)

  def join("texas:main", %{"cookies" => cookies}, socket) do
    uuid = extract_uuid(cookies)
    socket = assign(socket, :texas_uuid, uuid)
    GenServer.call({:via, Registry, {ClientRegistry, socket.assigns.texas_uuid}}, {:update_socket, socket}) # |> IO.inspect label: "call to update socket"
    {:ok, socket}
  end

  def handle_info({:diff, diff}, socket) do
      push socket, "diff", diff
      {:noreply, socket}
  end

  def handle_in("main", %{"form_data" => payload}, socket) do
    %{"action" => action, "method" => method} = payload
    verb = String.to_atom(method)
    routes = @router.__routes__()
    [route|_] = Enum.filter(routes, & &1.path == action && &1.verb == verb)
    socket = apply(route.plug, route.opts, [socket, payload])
    GenServer.call({:via, Registry, {ClientRegistry, socket.assigns.texas_uuid}}, {:update_socket, socket}) # |> IO.inspect label: "call to update socket"
    {:reply, :ok, socket}
  end

  def handle_call(:socket, _from, socket) do
    {:reply, socket, socket}
  end

  defp extract_uuid(cookies) do
    cookies = cookie_to_querystring(cookies)
    %{"texas_uuid" => uuid} = URI.decode_query(cookies)
    uuid
  end

  defp cookie_to_querystring(cookies) do
    cookies
    |> String.replace("\s", "&")
    |> String.replace(";", "")
  end
end
