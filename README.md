[![Hex.pm](https://img.shields.io/hexpm/v/texas.svg?style=for-the-badge&label=Texas)](https://hex.pm/packages/texas)
[![npm](https://img.shields.io/npm/v/texasjs.svg?style=for-the-badge&label=TexasJS)](https://www.npmjs.com/package/texasjs)

# What is Texas?

Texas is a back-end Virtual DOM library.  It aims to overhaul how you work with phoenix's view layer so that you can spend more time developing on the server, rather than dealing with complex toolchains and build systems that offload work to the client.  The main goal for Texas is to create a library that significantly speeds up development and testing, but I believe this library can achieve many advancements over how we've been writing web apps, including: reduced data over the wire, faster response times, and graceful degredation to an application that continues to function in the abscence of any JS.

#### This project builds on top of [Phoenix](http://phoenixframework.org/), so you'll need a new phoenix project.  These install docs assume you're using phoenix 1.3+.

###### You can find an example lightweight application [here](https://gitlab.com/dgmcguire/example_texas_app).  The commits on the example project are just following the installation below.

# Installation

## 1. Add dependencies
```elixir
# file: ./mix.exs
  defp deps do
    [
      ...
      {:texas, "X.X.X"}, # latest should be denoted by the texas hex badge at the top
      ...
    ]
  end

# file: ./assets/package.json
  ...
  "dependencies": {
    ...
    "texasjs": "X.X.X", # latest should be denoted by the texasjs npm badge at the top
    ...
  },
```
install the deps 
 - $ `mix deps.get`
 - $ `cd assets/ && npm install`

## 2. setup your websocket channels and texasjs
```javascript
// file: ./assets/js/app.js

import socket from "./socket"
import texasjs from  "texasjs"
// texas needs access to the phoenix apps websocket
new texasjs(socket)
```
```elixir
# file: ./lib/<your_app>_web/channels/user_socket.ex
defmodule WorkingOnDocsWeb.UserSocket do
  ...
  use Texas.Socket
end
```

## 3. let texas know where your endpoint and router modules are via config and setup the templating engine to compile .tex files
```elixir
# file: config/config.ex
config :texas, pubsub: <YourApp>Web.Endpoint
config :texas, router: <YourApp>Web.Router
config :phoenix, :template_engines,
  tex:  Texas.TemplateEngine
```

## 4. insert the texas plug into your http pipeline and import some rendering functions texas gives you into the controllers you want to use with texas
```elixir
 # file: ./lib/<your_app>_web/router.ex

defmodule <YourAppWeb>.Router do
  ...

  pipeline :browser do
    ...
    Texas.Plug
  end

  ...
end

# file: ./lib/<your_app>_web/controllers/page_controller.ex
defmodule <YourApp>Web.PageController do
  ...
  import Texas.Controller

  ...
end
```

## 5. Creating a dynamic template and view
> note: the template needs to be `.tex` (or whatever you put in your config) to get run thru the texas template engine!

```elixir
# file: ./lib/<your_app>_web/templates/<your_view>/index.html.tex

<div data-texas="example_list">
  Some example prototype data that will be overwritten.
</div>

# file: ./lib/<your_app>_web/views/<your_view>.ex

defmodule <YourApp>Web.<YourView> do
  use <YourApp>Web, :view

  def data(conn) do
    %{ example_list: example_list(conn) }
  end

  def example_list(_conn) do
    {"div", [class: "some-class"], []}
  end
end
```

## 6. responding with that rendered view via a controller
```elixir
# file: ./lib/<your_app>_web/controllers/page_controller.ex
...
  def index(conn, _params) do
    texas_render conn, "index.html", [texas: <YourApp>Web.<YourView>.data(conn)]
  end
... 
```
